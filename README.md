[![author](https://img.shields.io/badge/author-adrianogvs-red.svg)](https://www.linkedin.com/in/adrianogvs) 
[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/Adrianogvs/002-Projects-Data-Science)


### Olá !👋

<br />

#### Meu nome é Adriano Vilela, atuo há mais de 6 anos na área de tecnologia da informação, formação acadêmica, bacharelado em Engenharia de Software e estou cursando MBA em Tecnologia para Negócios AI, Data Science e Big Data.

<p></p>

  
#### Atualmente exerço a função de Analista de Inteligência de Dados no Grupo Águia Branca, empresa brasileira do segmento de logística, com atuação em vários estados.

<br />
<br />


### Cursos Concluídos e Certificações
Item | Curso | Carga Horária | Certificados
-----|-----|-----|-----
01 | Bacharel em Engenharia de Software | 3600 Horas | <p align="left">https://bit.ly/engenharia--de--software</p>
02 | Bootcamp Data Engineering Com AWS & Cia | 18 Horas | <p align="left">https://bit.ly/engineering-com-aws</p>
03 | Gerenciamento de Projeto de Software | 120 Horas |  <p align="left">https://bit.ly/gerenciamento-projeto-software</p>
04 | Bootcamp Desenvolvedor Business Intelligence | 148 Horas | <p align="left">https://bit.ly/business--intelligence</p>
05 | Bootcamp Ciencias de Dados | 148 Horas | <p align="left">https://bit.ly/ciencias-de-dados</p>

<br />
<br />

### Cursos em Andamento
Item | Curso | Carga Horária 
---|---|---
01 | MBA em Tecnologia para Negócios AI, Data Science e Big Data. | 660 Horas
02 | Data Science em Produção | 452 Horas
03 | Bootcamp Desenvolver Python | 148 Horas
04 | Microsoft Power BI Para Data Science, Versão 2.0 | 72 Horas
05 | Python Fundamentos Para Análise de Dados 3.0 | 60 Horas



<br />
<br />

### Ferramentas e Tecnologias
<div align="center">
  <br />
<img src="https://img.icons8.com/nolan/64/github.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original-wordmark.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/oracle/oracle-original.svg" width="40" height="40"/> &ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original-wordmark.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original-wordmark.svg" width="40" height="40" /> &ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/microsoft-sql-server.png" width="40" height="40" />&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jenkins/jenkins-original.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/putty/putty-original.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/docker/docker-original-wordmark.svg" width="40" height="40" />&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/filezilla/filezilla-plain.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/linux/linux-original.svg" width="40" height="40" />&ensp;&ensp;
<img src="https://img.icons8.com/plasticine/100/000000/oracle-pl-sql--v3.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/tableau-software.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original-wordmark.svg" width="40" height="40"/>&ensp;&ensp;<p></>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/r/r-original.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/ubuntu/ubuntu-plain-wordmark.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/visualstudio/visualstudio-plain.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original-wordmark.svg" width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/microsoft-excel-2019--v1.png"  width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/96/000000/power-bi.png"  width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/dusk/64/000000/dbeaver.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/virtualbox.png"  width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/fluency/48/000000/anaconda--v2.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/pycharm.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/html-5--v1.png"  width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/color/48/000000/css3.png"  width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/fluency/48/000000/jupyter.png" width="40" height="40"/>&ensp;&ensp;
<img src="https://img.icons8.com/ios/50/000000/sql.png" width="40" height="40"/>&ensp;&ensp;
</div>

<br />
<br />

### Estatisticas GitHub
<div align="center">
  
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=adrianogvs&layout=compact&langs_count=7&theme=highcontrast"/>
  <p>
  <p>
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=adrianogvs&show_icons=true&theme=highcontrast&include_all_commits=true&count_private=true"/>
<a href="https://github.com/adrianogvs">

</div

<br />
<br />

### Contatos:
<p>
<p>
<p>
<div align="center">
<a href="https://www.linkedin.com/in/adrianogvs/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>   <a href="https://www.instagram.com/adriano.gvs/" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a> <a href = "mailto:adriano.silva@alunos.unicesumar.edu.br"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
</div>






<!--
**Adrianogvs/Adrianogvs** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
